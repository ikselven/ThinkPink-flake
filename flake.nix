{
  description = "A very basic flake with flake-utils";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    utils,
    nixpkgs,
    ...
  } @ inputs:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in rec {
      packages = utils.lib.flattenTree {
        ThinkPink = pkgs.qt6Packages.callPackage ./default.nix {};
      };

      defaultPackage = packages.ThinkPink;

      apps = rec {
        ThinkPink = utils.lib.mkApp {
          drv = self.packages.${system}.ThinkPink;
        };
        default = ThinkPink;
      };
    });
}
