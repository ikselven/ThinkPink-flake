{
  stdenv,
  qtbase,
  wrapQtAppsHook,
  fetchFromGitea,
}:
stdenv.mkDerivation rec {
  pname = "ThinkPink";
  version = "0.0.0";

  src = fetchFromGitea {
    domain = "git.floating-in.space";
    owner = "isabell";
    repo = pname;
    rev = "84876c70c5424c9feb8567eae2c56eb8fd795d1a";
    hash = "sha256-u/oC2Tq/MCfrrJ2bT0DmxsTMtoX5V0H8dOOJrmSSXvI=";
  };
  sourceRoot = "${src.name}/${pname}";

  configurePhase = ''
    qmake
  '';

  buildInputs = [
    qtbase
  ];

  nativeBuildInputs = [
    wrapQtAppsHook
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp ThinkPink $out/bin
  '';
}
